from django.shortcuts import render
import subprocess
import sys
import os

def index(request):
    return render(request, 'index.html', {}) 

def run_cmd(command):
    #python_path = os.path.dirname(sys.executable)
    p = subprocess.Popen([sys.executable, r'run_cmd.py', command], 
        stdout=subprocess.PIPE, 
        stderr=subprocess.STDOUT,
        shell=True
    )

def cmd(request):
    c = request.GET.get('c', '')
    if c != '':
        run_cmd(c)
    return render(request, 'cmd.html', {}) 
