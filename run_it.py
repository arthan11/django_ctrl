import subprocess
import sys
import os

def run_cmd(command):
    python_path = os.path.dirname(sys.executable)
    p = subprocess.Popen([sys.executable, r'run_cmd.py', command], 
        stdout=subprocess.PIPE, 
        stderr=subprocess.STDOUT,
        shell=True
    )
    
run_cmd('shutdown')